import board

from kb import KMKKeyboard

from kmk.keys import KC
from kmk.modules.encoder import EncoderHandler
from kmk.extensions.statusled import statusLED
from kmk.modules.layers import Layers
from kmk.extensions.media_keys import MediaKeys
from kmk.modules.mouse_keys import MouseKeys
from kmk.modules.split import Split, SplitSide, SplitType


keyboard = KMKKeyboard()

keyboard.extensions.append(statusLED)
keyboard.modules.append(Layers())
keyboard.extensions.append(MediaKeys())
keyboard.modules.append(MouseKeys())
split = Split(split_side=SplitSide.LEFT)
keyboard.modules.append(split)
split = Split(
    split_flip=True,  # If both halves are the same, but flipped, set this True
    split_side=None,  # Sets if this is to SplitSide.LEFT or SplitSide.RIGHT, or use EE hands
    split_type=SplitType.UART,  # Defaults to UART
    split_target_left=True,  # Assumes that left will be the one on USB. Set to False if it will be the right
    uart_interval=20,  # Sets the uarts delay. Lower numbers draw more power
    data_pin=board.TX,  # The primary data pin to talk to the secondary device with
    data_pin2=board.RX,  # Second uart pin to allow 2 way communication
    uart_flip=False,  # Reverses the RX and TX pins if both are provided
    use_pio=False,  # Use RP2040 PIO implementation of UART. Required if you want to use other pins than RX/TX
)
layers = Layers()
mystatusLED = statusLED(led_pins=[board.A0, board.A1, board.MISO])
encoder_handler = EncoderHandler()
keyboard.modules = [layers, encoder_handler]
encoder_handler.pins = ((board.A3 , board.A2, None, 4), )

keyboard.keymap = [
        [
            KC.A,
        ]
    ]

# Rotary Encoder (1 encoder / 1 definition per layer)
encoder_handler.map = [ ((KC.VOLD, KC.VOLU),), # L1
                        ((KC.MW_UP, KC.MW_DOWN),), # L2
                        ((KC.A, KC.Z),), # L3
                        ((KC.A, KC.Z),), # L4
                        ]

if __name__ == '__main__':
    keyboard.go()

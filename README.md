# 25plus - DIY keyboard

40% -ish split keyboard inspired by 40percentclub/25 keeb
Uses kb2040 and KMK
Adds 3 more switches, encoder (with switch) and vertical stagger compared with 25

At this point PCB design is done, and should be good to go. I need to check rx/tx pins for serial communication between both halfs. 

## Roadmap
* Add kmk frimware
* Prototype
* Create 3d design for the case

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## License
Licensed on GPL3 terms

## Project status
One girl hobby project, don't expect too much ;P
